(function ($, Drupal, once, document, window) {
  'use strict';
  Drupal.behaviors.focusPoint = {
    attach: function (context, drupalSettings) {

      const focuspoints = once('focuspointfocusbehavior', '.focuspoint', context);
      focuspoints.forEach(function (focuspoint) {
        $(focuspoint).focusPoint({
          throttleDuration: 100
        }).adjustFocus();
      });

      let hasFocalpointBreakpoints = drupalSettings.hasOwnProperty("focalpoint-breakpoints");
      if (hasFocalpointBreakpoints){
        $(window).on("orientationchange resize load", focuspointBreakpointsRespond);
      }

    }
  };

  let focuspointBreakpointsRespond = function () {
    let focuspointBreakpoints = drupalSettings['focalpoint-breakpoints'];

    // Loop through each item_namespace for each focuspoint-{{ focalpoint.field_name }}.
    for (let itemNamespace in focuspointBreakpoints) {
      let breakpoints = focuspointBreakpoints[itemNamespace];
      let focuspoints = $('.focuspoint-' + itemNamespace);

      focuspoints.each(function (index, focuspoint) {
        let $focuspoint = $(focuspoint);
        for (let breakpoint in breakpoints) {
          let { mediaQuery, height, label } = breakpoints[breakpoint];
          // Check if the media query matches the current window width.
          if (window.matchMedia(mediaQuery).matches) {
            // Apply the height to the focuspoint element.
            $focuspoint.css('height', height + 'px').attr({'data-active-breakpoint': label, 'data-height': height});
            // Adjust focus for the current breakpoint.
            $focuspoint.focusPoint().adjustFocus();
          }
        }
      });
    }
  };

})(jQuery, Drupal, once, document, window);
