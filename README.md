# Focal Point Focus

An Image Formatter for Focal Point crops integrating with jquery-focuspoint plugin for
displays. Set expected image render height (per view mode) and let the image shift to
maintain chosen focal point view-ability.

- HTML output uses figure tag. Does use Twig template, override as you need!
- If image field 'title' it is rendered as figcaption (normally title is used as an
attribute on an image tag).
- 'mute title' bypasses 'figcaption' ready output of 'title'
- 'first-only' display options useful for multivalue use-cases.
- 'Lazy Load' attribute options:  none, lazy, eager.
[Browser-level image lazy loading for the web](https://web.dev/browser-level-image-lazy-loading)
- Image field 'alt' text is supported.
- Style Scoped for breakpoints (in twig output).
- Breakpoints height enhanced by Javascript Behaviour.

## Requirements
1. Drupal!
2. [https://www.drupal.org/project/crop](url)
3. [https://www.drupal.org/project/focal_point](url)

## Soft Dependency
1. Breakpoint (drupal core) module.  This module does not enable Breakpoints.  Enable if
you wish to use the Focal Point Focus Breakpoint feature!  Configure as you need!


## Installation
  ### Manually:
  1. install into /modules.
  2. enable module.
  3. setup Image field as you need. see 'Configuation' below!

  ### Composer:
  1. From project base folder of you Composer ready project type 'composer require drupal/focal_point_focus'
  2. enable module.
  3. setup Image field as you need. see 'Configuation' below!


## Configuration
- The Field Widget 'Form Display' should be expected to use the 'crop thumbnail' Preview
Image Style.  Others will probably will not work well with the reposition math.
- 'Manage Display' View Mode needs to have the expected Display Height set. this is key
to working with responsive widths in the region of where the content will be rendered.
- Breakpoints Special Tips:
   - '-1' will use the Default Height
   - '0/Empty' will mute the individual clause from output.

## Troubleshooting

- Upgrading from 2.0.x:  Breakpoints will default to 'none' giving you time to individually
update all Field Display Configurations before this new feature will be seen.

- Breakpoint Tip 1:  Use the Breakpoint Group that the theme the field will be rendered in
will be using  (Formatters have no connection to 'current/active' theme).

- Breakpoint Tip 2:  You can use any combination of Groups on the same Bundle using
Different Field!  Even set different heights for the same group between field instances!

- *Breakpoint Tip 3: Provided items will used by Style, not Picture.
    - EXAMPLE: `<style>@media {{ PROVIDED-ITEM }} {}</style>`
    - NOT IMAGECACHE: `<Picture><Source media="{{ PROVIDED-ITEM }}"><img scr=""></picture>`

- Breakpoint Tip 4*:  Responsibility for working Breakpoints Groups mediaQuery is the
responsibility of the Provider Module, not this module!

- Breakpoint Tip 5*:  be sure to understand the purpose of each breakpoint being
provided in a set.  The reason you may want to MUTE (0/empty) one (or any) of the
individual items will depend on that understanding of the provided items
in actual use-cases.

- Breakpoint Tip 6:  You may need to 'provide your own' breakpoint Group into your own
custom module, or into your custom theme.  Please review
   - [Working with Breakpoint in Drupal](https://www.drupal.org/node/1803874)
   - [developer.mozilla.org | Using media queries](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_media_queries/Using_media_queries)
   - [w3schools.com | Responsive Web Design - Media Queries](https://www.w3schools.com/css/css_rwd_mediaqueries.asp)

- Special Sitebuilder/dev Breakpoint Provider testing can be enabled/disabled via:
    - \Drupal::state()->set('focal_point_focus.breakpoint_test',true);
    - \Drupal::state()->delete('focal_point_focus.breakpoint_test');

> This will add an 'Dashed Outline' the cycles through colors to help visually test the
> items.  Your Active Provider has set in Their plugin definition (ie:  if they work,
> then you will see the outline colours cycle from their inline Scoped Style (through
> Twig template).  Has 20 colors/items ready as max expected test items from a provider source.
