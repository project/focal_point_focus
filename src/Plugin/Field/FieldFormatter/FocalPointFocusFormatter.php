<?php

namespace Drupal\focal_point_focus\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\focal_point\FocalPointManager;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'focal_point_focus' formatter.
 *
 * @FieldFormatter(
 *   id = "focal_point_focus",
 *   label = @Translation("Focal Point Focus"),
 *   field_types = {
 *     "image"
 *   },
 *   quickedit = {"editor" = "disabled"}
 * )
 */
class FocalPointFocusFormatter extends ImageFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * Focal point manager object.
   *
   * @var \Drupal\focal_point\FocalPointManager
   */
  protected $focalPointManager;

  /**
   * Constructs an ImageFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Third party settings.
   * @param array $focal_point
   *   FocalPointManager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, FocalPointManager $focal_point_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->focalPointManager = $focal_point_manager;
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      new FocalPointManager($container->get('entity_type.manager')),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $first_only = $this->getSetting('first-only') == FALSE ? $this->t('Show all') : $this->t('First only.');
    $summary[] = $this->t('First Image Only: @value', ['@value' => $first_only]);
    $title = $this->getSetting('title') == TRUE ? $this->t('Mute') : $this->t('Show Title');
    $summary[] = $this->t('Mute Title: @value', ['@value' => $title]);
    $summary[] = $this->t('Display Height: @valuepx', ['@value' => $this->getSetting('height')]);
    $loading = $this->getSetting('loading')!='' ? $this->getSetting('loading') :'None';
    $summary[] = $this->t('Lazy Loading: @value', ['@value' =>  $loading ]);

    if (\Drupal::hasService('breakpoint.manager')) {
      $currentSelectedProvider = $this->getSetting('focal-provider');
      if ($currentSelectedProvider != 'none' || $currentSelectedProvider != '') {

        $breakpoint_heights = $this->getSetting('breakpoint_heights');
        $breakpoints_manager = \Drupal::service('breakpoint.manager');
        $breakpoints = $breakpoints_manager->getDefinitions();
        $activeBreakpointGroup = $breakpoints_manager->getBreakpointsByGroup($currentSelectedProvider);
        $first = reset($activeBreakpointGroup);
        if ($first) {
          $summary[] = $this->t('<B>Breakpoints Group:  @group</b>', ['@group' => $first->getGroup()]);
        }
        foreach ($activeBreakpointGroup as $provider => $config) {
          $provider = $this->makeFieldName($config->getGroup());
          $breakpoint = $this->makeFieldName($provider . '.' . $config->getLabel());
          $height = (isset($breakpoint_heights[$provider]) && isset($breakpoint_heights[$provider][$breakpoint])) ? (int) $breakpoint_heights[$provider][$breakpoint] : '';
          switch ($height) {
            case -1:
              // Use the basic degrade height.
              $height = $final_height;
              $height = 'uses default [' . $this->getSetting('height') . 'px]';
              $summary[] = $this->t('&nbsp;&bull;&nbsp;<abbr title="@mediaQuery ">@breakpoint</abbr>:  @value', ['@breakpoint' => $config->getLabel(), '@mediaQuery' => $config->getMediaQuery(), '@value' => $height]);
              break;

            case FALSE:
            case NULL:
            case '':
            case 0:
              // Secondary reset to nullify item.
              $summary[] = $this->t('&nbsp;&bull;&nbsp;<abbr title="@mediaQuery ">@breakpoint</abbr>:  will be muted.', ['@breakpoint' => $config->getLabel(), '@mediaQuery' => $config->getMediaQuery()]);
              break;

            default:
              $summary[] = $this->t('&nbsp;&bull;&nbsp;<abbr title="@mediaQuery ">@breakpoint</abbr>:  @valuepx', ['@breakpoint' => $config->getLabel(), '@mediaQuery' => $config->getMediaQuery(), '@value' => $height]);

              break;
          }
        }
      }
      else {
        $summary[] = $this->t('No Breakpoints Group used.');
      }
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'height' => 300,
      'title' => FALSE,
      'first-only' => FALSE,
      'focal-provider' => 'none',
      'loading' => FALSE,
      'breakpoint_heights' => [],
    ] + parent::defaultSettings();
  }

  /**
   * Settings form.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state array.
   *
   * @return mixed
   *   Returns mixed data.
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $element['first-only'] = [
      '#title' => $this->t('Only show first image (if multivalue)'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('first-only'),
    ];
    $element['title'] = [
      '#title' => $this->t('Mute Title (figcaption)'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('title'),
    ];
    $element['loading'] = [
      '#title' => $this->t('<q>Lazy Loading</q> for this template'),
      '#type' => 'select',
      '#options' => [
        ''=>$this->t('No Loading attribute'),
        'lazy'=>$this->t('lazy'),
        'eager'=>$this->t('eager'),
      ],
      '#default_value' => $this->getSetting('loading'),
      '#description' => 'Helpful Lazy Loading Tips: <a href="https://web.dev/browser-level-image-lazy-loading" target="_blank">Browser-level image lazy loading for the web</a>.'
    ];
    if (\Drupal::hasService('breakpoint.manager')) {
      $element['breaker'] = [
        '#markup' => '<hr>',
        '#type' => 'markup',
      ];
    }
    $element['height'] = [
      '#title' => $this->t('Display Height'),
      '#type' => 'number',
      '#field_suffix' => 'px',
      '#attributes' => ['min' => 2, 'size' => 9],
      '#default_value' => $this->getSetting('height'),
      '#element_validate' => [[$this, 'validateHeight']],
    ];

    $providers = [];
    if (\Drupal::hasService('breakpoint.manager')) {
      // Add desc to Height.
      $element['height']['#description'] = $this->t('This is the general fallback height (if -1 used in Breakpoint Provider).');

      $breakpoints_manager = \Drupal::service('breakpoint.manager');
      $breakpoints = $breakpoints_manager->getDefinitions();
      $nestedElements = [];
      foreach ($breakpoints as $breakpoint_id => $breakpoint) {
        $breakpoint_id = $this->makeFieldName($breakpoint_id);
        $provider = $this->makeFieldName($breakpoint['group']);
        $providers[$provider] = $breakpoint['provider'];
        $states = [
          'visible' => ['[data-focalpoint-provider-selector="focalpoint-provider-instance-select"]' => ['value' => $provider]],
        ];
        $breakpoint_height = $this->getSetting('breakpoint_heights') ?
           ($this->getSetting('breakpoint_heights')[$provider] && $this->getSetting('breakpoint_heights')[$provider][$breakpoint_id])
           ? $this->getSetting('breakpoint_heights')[$provider][$breakpoint_id] : FALSE
          : FALSE;
        // Get a group only if first of group.
        if (!isset($nestedElements['breakpoint_heights'][$provider])) {
          $nestedElements['breakpoint_heights'][$provider] = [
            '#type' => 'fieldset',
            '#title' => $this->t('Breakpoint Group:  <b>@provider</b>', ['@provider' => $breakpoint['provider']]),
            '#states' => $states,
            '#description' => $this->t('<p>Special Cases:</p><ul>
            <li>Using -1 (minus one) will use the current fallback height.</li>
            <li>Using 0 (zero) or an empty number will mute the breakpoint condition from output.</li></ul>') . '<hr />',
            '#description_display' => 'before',
          ];
        }
        $nestedElements['breakpoint_heights'][$provider][$breakpoint_id] = [
          '#type' => 'number',
          '#title' => $this->t('Display Height for <q>@breakpoint</q>', ['@breakpoint' => $breakpoint['label']]),
          '#default_value' => $breakpoint_height,
          '#attributes' => ['min' => -1, 'size' => 9],
          '#field_suffix' => 'px',
          '#description' => $this->t('<code>@media @mediaQuery</code>', ['@mediaQuery' => $breakpoint['mediaQuery']]),
          '#description_display' => 'before',
        ];
      }

      \ksort($providers);
      $element['focal-provider'] = [
        '#title' => $this->t('Breakpoint Integration'),
        '#type' => 'select',
        '#attributes' => ['data-focalpoint-provider-selector' => 'focalpoint-provider-instance-select'],
        '#options' => ['none' => $this->t('— None —')] + $providers,
        '#default_value' => $this->getSetting('focal-provider'),
        '#description' => $this->t('Selects which Breakpoint Group to use.'),
        '#description_display' => 'before',
      ];
      $element += $nestedElements;
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $breakpointColorTest = $this->breakpointTestColors();
    $field_definition = $items->getFieldDefinition();
    // Get the field item list's parent entity.
    $entity = $items->getEntity();
    $field_instance_id = $field_definition->getFieldStorageDefinition()->id();
    $bundles = \implode(',', $field_definition->getFieldStorageDefinition()->getBundles());
    $currentSelectedProvider = $this->getSetting('focal-provider');

    $item_namespace = $this->makeFieldName($field_instance_id . '.' . $bundles . '-' .$this->viewMode . '-' . $currentSelectedProvider);
    $element = [];
    $properties = [];
    $first_only = $this->getSetting('first-only');
    $title = $this->getSetting('title');
    $final_height = $this->getSetting('height');
    $breakpoints_height = $this->getSetting('breakpoint_heights');
    $files = $this->getEntitiesToView($items, $langcode);
    // Early opt-out if the field is empty.
    if (empty($files)) {
      return [];
    }
    // Carry basic image info.
    foreach ($items as $delta => $item) {
      if ($first_only && $delta != 0) {
        continue;
      }
      $this_image = $item->getValue();
      $properties[$delta]['loading'] = $this->getSetting('loading');
      $properties[$delta]['target_id'] = $this_image['target_id'];
      $properties[$delta]['width'] = $this_image['width'];
      $properties[$delta]['height'] = $this_image['height'];
      $properties[$delta]['alt'] = $this_image['alt'];
      $properties[$delta]['title'] = $title == FALSE ? $this_image['title'] : '';
      $properties[$delta]['field_name'] = $item_namespace;
      $properties[$delta]['focal_provider'] = $currentSelectedProvider;
    }

    foreach ($files as $delta => $file) {
      if ($first_only && $delta != 0) {
        continue;
      }
      $properties[$delta]['display_height'] = $final_height;
      $file_uri = $file->getFileUri();
      $properties[$delta]['src'] = \Drupal::service('file_url_generator')->generateAbsoluteString($file_uri);
      $crop = $this->focalPointManager->getCropEntity($file, 'focal_point');

      if ($crop) {
        $focal_point = $crop->position();
        $properties[$delta]['focal_point_x'] = $focal_point['x'];
        $properties[$delta]['focal_point_y'] = $focal_point['y'];
        $centerX = $this_image['width'] / 2;
        $centerY = $this_image['height'] / 2;
        $properties[$delta]['x'] = \round(($focal_point['x'] / $this_image['width'] - .5) * 2, 4);
        $properties[$delta]['y'] = \round(($focal_point['y'] / $this_image['height'] - .5) * -2, 4);
      }
      else {
        /* No crop!  formatter has been added to image that is not a crop. therefore: render with x|y in image center.. */
        $properties[$delta]['focal_point_x'] = 0;
        $properties[$delta]['focal_point_y'] = 0;
      }
      // Adjust the height of the image based on the selected breakpoint.
      $css = '';
      if (\Drupal::hasService('breakpoint.manager') && $crop) {
        $attached = [];
        $breakpointTest = \Drupal::state()->get('focal_point_focus.breakpoint_test');
        $colorItem = 0;
        $breakpoints_manager = \Drupal::service('breakpoint.manager');
        // Load actual breakpoint data state for current weight.
        $breakpoints = $breakpoints_manager->getBreakpointsByGroup($currentSelectedProvider);
        foreach ($breakpoints as $breakpoint_id => $breakpoint) {
          $breakpoint_id = $this->makeFieldName($breakpoint_id);
          $mediaQuery = $breakpoint->getMediaQuery();
          $label = $breakpoint->getLabel()->__toString();
          $height = $breakpoints_height[$currentSelectedProvider][$breakpoint_id];
          if ($height !== NULL) {
            if (empty($css)) {
              $css .= ".focuspoint-{$item_namespace} {height:{$final_height}px;}" . PHP_EOL;
            }
            // Handle special treatment of -1 and empty/none.
            switch ($height) {
              case -1:
                // Use the basic degrade height.
                $height = $final_height;
                break;

              case FALSE:
              case NULL:
              case '':
              case 0:
                // Secondary reset to nullify item.
                $height = NULL;
                break;
            }
            if ($height !== NULL) {
              $weight = $breakpoint->getWeight();
              if ($breakpointTest) {
                $breakpointTest = " outline:2px dashed " . $color[$colorItem];
              }
              $attached[$item_namespace][$weight] = ['mediaQuery' => $mediaQuery, 'height' => $height, 'label' => $label, 'weight' => $weight];
              $css .= "@media {$mediaQuery} { /* {$label} */" . PHP_EOL;
              $css .= "  .focuspoint-{$item_namespace} {height: {$height}px;{$breakpointTest}}" . PHP_EOL;
              $css .= "}" . PHP_EOL;
              $colorItem++;
            }
            else {
              $css .= "/* {$label} muted */" . PHP_EOL;

            }
          }

        }
        if (!empty($css)) {
          $css = \trim($css, PHP_EOL);
          $properties[$delta]['css'] = $css;
        }
      }
      if (!empty($attached)) {
        // Sort the breakpoints based on the "weight" property.
        $pre = $attached;
        \uasort($attached, function ($a, $b) {
          return $a['weight'] <=> $b['weight'];
        });
        $element['#attached']['drupalSettings']['focalpoint-breakpoints'] = $attached;
      }

      $element[$delta] = [
        '#theme' => 'focal_point_focus',
        '#focalpoint' => $properties[$delta],
      ];
    }
    return $element;
  }

  /**
   * Custom validation callback for the 'height' field.
   */
  public function validateHeight($element, FormStateInterface $form_state) {
    $height = $form_state->getValue($element['#parents']);
    if ($height < 2) {
      $form_state->setError($element, $this->t('The Display Height cannot be less than 2px.'));
    }
  }

  /**
   * Parse string to a field safe name.
   */
  public function makeFieldName($str) {
    return Html::cleanCssIdentifier($str, ['.' => '--']);
  }

  /**
   * List of the 20 colours used by the breakpoint_text color wheel.
   */
  public function breakpointTestColors() {
    return [
      'BlueViolet',
      'lime',
      'fuchsia',
      'springgreen',
      'hotpink',
      'lightgoldenrod',
      'DarkGoldenRod',
      'DarkOrange',
      'DeepPink',
      'yellow',
      'CornflowerBlue',
      'crimson',
      'lavenderblush',
      'olive',
      'maroon',
      'darkcyan',
      'greenyellow',
      'mediumvioletred',
      'sienna',
      'yellowgreen',
    ];
  }

}
